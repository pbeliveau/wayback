# Archive urls to Wayback Machine
This is a small script to archive a url to the Wayback Machine.
The archived url is pasted to your clipboard.


![usage](https://i.imgur.com/ZeNlGKD.png)

## Batch export
If you have multiple urls to archive, you can remove line 7 to 9
since I doubt very much you will need to add the archived urls to the clipboard.
