#!/usr/bin/env bash

# Sumbit url to wayback machine
URL="$1"
WAYBACK=$(curl -s https://web.archive.org/save/{$URL})

# Latest snapshot to clipboard
SNAPSHOT=$(curl -s https://archive.org/wayback/available?url={$URL})
echo $SNAPSHOT | awk -F\" '{print $18}' | xclip -selection clipboard
